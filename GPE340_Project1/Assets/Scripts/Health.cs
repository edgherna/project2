using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    public int maxHealth = 100;
    public int currentHealth;

    public HealthBar healthBar;

    private void Start()
    {
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
        GameObject.Find("LifeText").GetComponent<LifeTracker>().lifeCount = 2;
        GameObject.Find("LifeText").GetComponent<LifeTracker>().UpdateLifeCounterUI();
    }

    private void Update()
    {
        healthBar.SetHealth(currentHealth);
        /*
        if (Input.GetKeyDown(KeyCode.Z))
        {
            TakeDamage(20);
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            HealDamage(20);
        }
        */

    }

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;

        healthBar.SetHealth(currentHealth);
    }

    public void HealDamage(int recoverAmount)
    {
        currentHealth += recoverAmount;

        healthBar.SetHealth(currentHealth);
    }
}
