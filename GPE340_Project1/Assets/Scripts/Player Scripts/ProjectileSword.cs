using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Audio;

public class ProjectileSword : MonoBehaviour
{
    /*
    This is the name of the projectile that you are inserting to the object. 
    */
    public GameObject bullet;

    /*
    This is the bullet force.
    */
    public float shootForce, upwardForce;

    /*
    These are the stats for the projectiles that are going to be released by the object
    that has this script attached.
    */
    public float timeBetweenShooting, spread, reloadTime, timeBetweenShots;
    public int magazineSize, bulletsPerTap;
    public bool allowButtonHold;

    int bulletsLeft, bulletsShot;

    /*
    Statuses 
    */
    bool shooting, readyToShoot, reloading;

    /*
    References 
    */
    public Camera fpsCam;
    public Transform attackPoint;
    bool isTopDown = true;

    /*
    Graphics 
    */
    public GameObject muzzleFlash;
    public TextMeshProUGUI ammunitionDisplay;

    /*
    Sounds 
    */
    public AudioClip sound;

    /*
    This below is just for some debugging. 
    */
    public bool allowInvoke = true;

    private void Awake()
    {
        bulletsLeft = magazineSize;
        readyToShoot = true;
    }

    private void Update()
    {
        MyInput();

        /*
        This is to set an ammo display, if it exists 
        */
        if (ammunitionDisplay != null)
        {
            ammunitionDisplay.SetText(bulletsLeft / bulletsPerTap + " / " + magazineSize / bulletsPerTap);
        }
    }

    private void MyInput()
    {
        /*
        This is to check if you can hold down the button. 
        */
        if (allowButtonHold)
        {
            sound.LoadAudioData();
            shooting = Input.GetKey(KeyCode.Mouse0);
        }
        else
        {
            sound.LoadAudioData();
            shooting = Input.GetKeyDown(KeyCode.Mouse0);
        }

        /*
        This is to reload for your bullets 
        */
        if (Input.GetKeyDown(KeyCode.R) && bulletsLeft < magazineSize && !reloading)
        {
            Reload();
        }

        if (readyToShoot && shooting && !reloading && bulletsLeft <= 0)
        {
            Reload();
        }

        /*
        This is where you are shooting.
        */
        if (readyToShoot && shooting && !reloading && bulletsLeft > 0)
        {
            bulletsShot = 0;
            Shoot();
        }

        //This is for the topDownCamera
        /*
        If c button is pressed, we change the camera.
        */
        
        if (Input.GetKeyDown(KeyCode.C))
        {
            if (isTopDown)
            {
                GameObject.Find("LevelCamera").GetComponent<Camera>().enabled = false;
                GameObject.Find("PlayerKnight").GetComponent<InputHandler>().enabled = false;
                GameObject.Find("PlayerKnight").GetComponent<TopDownCharacterView>().enabled = false;
                Cursor.lockState = CursorLockMode.Locked;
                isTopDown = false;
            }
            else
            {
                GameObject.Find("LevelCamera").GetComponent<Camera>().enabled = true;
                GameObject.Find("PlayerKnight").GetComponent<InputHandler>().enabled = true;
                GameObject.Find("PlayerKnight").GetComponent<TopDownCharacterView>().enabled = true;
                Cursor.lockState = CursorLockMode.None;
                isTopDown = true;
            }
        }
    }

    private void Shoot()
    {
        readyToShoot = false;

        /*
        This is to find the exact hitting position of your projectiles
        */
        Ray ray = fpsCam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;

        Vector3 targetPoint;
        if (Physics.Raycast(ray, out hit))
        {
            targetPoint = hit.point;
        }
        else
        {
            targetPoint = ray.GetPoint(75);
        }

        /*
        Calculates the direction from attackPoint to targetPoint. 
        */
        Vector3 directionWithoutSpread = targetPoint - attackPoint.position;

        /*
        Calculates the spread.
        */
        float x = Random.Range(-spread, spread);
        float y = Random.Range(-spread, spread);

        /*
        Calculates the new direction with spread. 
        */
        Vector3 directionWithSpread = directionWithoutSpread + new Vector3(x, y, 0);

        /*
        Instantiate the bullet/projectile of the object with this script. 
        */
        GameObject currentBullet = Instantiate(bullet, attackPoint.position, Quaternion.identity);

        /*
        This will rotate the bullet to the shooting direction. 
        */
        currentBullet.transform.forward = directionWithSpread.normalized;

        /*
        After the bullet is spawned, this adds force to the bullet. 
        */
        currentBullet.GetComponent<Rigidbody>().AddForce(directionWithSpread.normalized * shootForce, ForceMode.Impulse);
        currentBullet.GetComponent<Rigidbody>().AddForce(fpsCam.transform.up * upwardForce, ForceMode.Impulse);

        /*
        If we do have a muzzle flash, this will instantiate it. 
        */
        if (muzzleFlash != null)
        {
            Instantiate(muzzleFlash, attackPoint.position, Quaternion.identity);
        }

        bulletsLeft--;
        bulletsShot++;

        /*
        Invoke resetShot function (if not already invoked), with your timeBetweenShooting
        */
        if (allowInvoke)
        {
            Invoke("ResetShot", timeBetweenShooting);
            allowInvoke = false;
        }


    }

    private void ResetShot()
    {
        readyToShoot = true;
        allowInvoke = true;
    }

    /*
    This module is the reload button to be able to recharge after firing a number of times.
    */
    private void Reload()
    {
        reloading = true;
        Invoke("ReloadFinished", reloadTime);
    }
    /*
    This module is to finish the reloading process to be able to shoot again. 
    */
    private void ReloadFinished()
    {
        bulletsLeft = magazineSize;
        reloading = false;
    }
}
