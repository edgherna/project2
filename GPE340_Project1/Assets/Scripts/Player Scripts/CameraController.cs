using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    //These are the variables of the Camera that can be adjusted.
    [SerializeField] private float mouseSensitivity;

    //References
    private Transform parent;

    private void Start()
    {
        /*
        When the game starts, the data is stored into the object's camera and the camera will lock with the mouse's pointer.
        */
        parent = transform.parent;
        Cursor.lockState = CursorLockMode.None;
    }

    private void Update()
    {
        Rotate();
    }

    private void Rotate()
    {
        /*
        Every time that this module is called, the camera will always stay behind the player. This also controls
        where the player is facing.
        */
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;

        parent.Rotate(Vector3.up, mouseX);
    }
}