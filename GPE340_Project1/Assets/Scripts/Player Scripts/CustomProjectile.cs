using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomProjectile : MonoBehaviour
{
    public Rigidbody rb;
    public GameObject explosion;
    public LayerMask whatIsEnemies;

    /*
    These are the stats of the projectiles to adjust. 
    */
    [Range(0f, 1f)]
    public float bounciness;
    public bool useGravity;

    /*
    Damage 
    */
    public int explosionDamage;
    public float explosionRange;

    /*
    These three are to help determine the lifetime of the projectile. 
    */
    public int maxCollisions;
    public float maxLifetime;
    public bool explodeOnTouch = true;

    int collisions;
    PhysicMaterial physic_mat;

    private void Start()
    {

    }

    private void Update()
    {
        /*
        This will tell you when the projectile will explode. 
        */
        if (collisions > maxCollisions)
        {
            Explode();
        }

        /*
        This is the countdown of the lifetime the projectile has. 
        */
        maxLifetime -= Time.deltaTime;
        if (maxLifetime <= 0)
        {
            Explode();
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        /*
        This will count the collisions. 
        */


        collisions++;

        if (collision.collider.CompareTag("Enemy") && explodeOnTouch)
        {
            Explode();
        }

        if (collision.collider.CompareTag("Player") && explodeOnTouch)
        {
            GameObject.Find("PlayerKnight").GetComponent<Health>().TakeDamage(explosionDamage);
            Explode();
        }
    }
    
    private void Setup()
    {
        /*
        Here, we're going to create a new physic material
        */
        physic_mat = new PhysicMaterial();
        physic_mat.bounciness = bounciness;
        physic_mat.frictionCombine = PhysicMaterialCombine.Minimum;
        physic_mat.bounceCombine = PhysicMaterialCombine.Maximum;

        /*
        Assign a material to collider 
        */
        GetComponent<SphereCollider>().material = physic_mat;

        rb.useGravity = useGravity;

    }

    private void Explode()
    {
        /*
        This is to Instantiate the explosion 
        */
        if (explosion != null)
        {
            Instantiate(explosion, transform.position, Quaternion.identity);
        }

        Collider[] enemies = Physics.OverlapSphere(transform.position, explosionRange, whatIsEnemies);
        for (int i = 0; i < enemies.Length; i++)
        {
            /*
            To test for multiple opponents 
            */
            enemies[i].GetComponent<EnemyAI>().TakeDamage(explosionDamage);
        }

        /*
        This is to add a small delay. 
        */
        Invoke("Delay", 0.01f);
    }

    private void Delay()
    {
        Destroy(gameObject);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, explosionRange);
    }

}