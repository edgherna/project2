using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagePlayer : MonoBehaviour
{
    GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision coli)
    {
        if (coli.collider.CompareTag("Enemy"))
        {
            player.GetComponent<Health>().currentHealth -= 20;
        }
    }
}
