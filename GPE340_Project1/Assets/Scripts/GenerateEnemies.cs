using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateEnemies : MonoBehaviour
{
    /*
    These are the basic stats that you will need for the enemy generator.
    */
    public GameObject theEnemy;
    public GameObject healthPack;
    public int xPos;
    public int zPos;
    public int enemyCount, maxCount;
    public int luck;
    public float timer;

    /*
    Under the Start module, you need to Start the Coroutine. 
    */
    private void Start()
    {
        StartCoroutine(EnemyDrop());
    }

    /*
    This IEnumerator module repeats the process of putting an enemy in the level. 
    */
    IEnumerator EnemyDrop()
    {
        while (enemyCount < maxCount)
        {
            //We're using a random int to determine if we spawn an enemy or it's a dud and spawn health instead.
            luck = Random.Range(1, 20);
            if (luck % 6 == 0)
            {
                /*
                Two random integers are assigned to be able to instantiate the healthpack 
                */
                xPos = Random.Range(1, 50);
                zPos = Random.Range(1, 31);
                Instantiate(healthPack, new Vector3(xPos, 0, zPos), Quaternion.identity);
                yield return new WaitForSeconds(timer);
            }
            else
            {
                /*
                Two random integers are assigned to be able to instantiate the enemies.
                The spawns will happen overtime as we make a small pause of 3 seconds before
                spawning in the next enemy
                */
                xPos = Random.Range(1, 50);
                zPos = Random.Range(1, 31);
                Instantiate(theEnemy, new Vector3(xPos, 0, zPos), Quaternion.identity);
                yield return new WaitForSeconds(timer);
                enemyCount += 1;
            }
        }
    }

}
