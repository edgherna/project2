using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI : MonoBehaviour
{
    /*
    This is the NavMesh that is required for the pathfinding for enemies to 
    roam around the level.
    */
    public NavMeshAgent agent;

    /*
    This is to help keep track of the player for the enemies to be able to aim
    their attacks towards the player.
    */
    public Transform player, attackPoint;

    /*
    These layer masks are what helps the enemy object determine the player and the ground. 
    */
    public LayerMask whatIsGround, whatIsPlayer;

    /*
    This is the enemy's health. 
    */
    public float health;

    /*
    These are the necessary declarations for the Patrolling State. 
    */
    public Vector3 walkPoint;
    bool walkPointSet;
    public float walkPointRange;

    /*
    These are the necessary declarations for the Attacking State. 
    */
    public float timeBetweenAttacks, turnSpeed;
    bool alreadyAttacked;
    public GameObject projectile;

    /*
    States
    */
    public float sightRange, attackRange;
    public bool playerInSightRange, playerInAttackRange;

    private void Awake()
    {
        /*
        Make sure you define the correct GameObject that the player will be controlling.
        If you somehow get a weird reaction or not attacking you at all, always double-check
        this line and the GameObject.
        */
        player = GameObject.Find("PlayerKnight").transform;
        agent = GetComponent<NavMeshAgent>();
    }

    private void Update()
    {
        /*
        Checks for sight and attack range.
        */
        playerInSightRange = Physics.CheckSphere(transform.position, sightRange, whatIsPlayer);
        playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, whatIsPlayer);

        if (!playerInSightRange && !playerInAttackRange)
        {
            Patrolling();
        }

        if (playerInSightRange && !playerInAttackRange)
        {
            ChasePlayer();
        }

        if (playerInSightRange && playerInAttackRange)
        {
            AttackPlayer();
        }
    }

    private void Patrolling()
    {
        if (!walkPointSet)
        {
            SearchWalkPoint();
        }

        if (walkPointSet)
        {
            agent.SetDestination(walkPoint);
        }

        Vector3 distanceToWalkPoint = transform.position - walkPoint;

        /*
        When this object reaches the Walkpoint. 
        */
        if (distanceToWalkPoint.magnitude < 1f)
        {
            walkPointSet = false;
        }
    }

    private void SearchWalkPoint()
    {
        float randomZ = Random.Range(-walkPointRange, walkPointRange);
        float randomX = Random.Range(-walkPointRange, walkPointRange);

        walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

        if (Physics.Raycast(walkPoint, -transform.up, 2f, whatIsGround))
        {
            walkPointSet = true;
        }
    }

    private void ChasePlayer()
    {
        agent.SetDestination(player.position);
    }

    private void AttackPlayer()
    {
        Vector3 direction = player.position - transform.position;
        /*
        This line makes sure that the object doesn't move when attacking the player. 
        */
        agent.SetDestination(transform.position);

        direction.y = 0;
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), turnSpeed * Time.deltaTime);
        //transform.LookAt(player);

        if (!alreadyAttacked)
        {
            Rigidbody rb = Instantiate(projectile, attackPoint.position, Quaternion.identity).GetComponent<Rigidbody>();
            rb.AddForce(transform.forward * 32f, ForceMode.Impulse);
            //rb.AddForce(transform.up * 8f, ForceMode.Impulse);

            alreadyAttacked = true;
            Invoke(nameof(ResetAttack), timeBetweenAttacks);
        }
    }

    private void ResetAttack()
    {
        alreadyAttacked = false;
    }

    public void TakeDamage(int damage)
    {
        health -= damage;

        if (health <= 0)
        {
            Invoke(nameof(DestroyEnemy), .1f);
        }
    }

    private void DestroyEnemy()
    {
        /*
        In these two codes below, to make sure that the UI is counting down the points,
        we find the GameObject that we named in the editor, get the component that tracks
        points and add to the killcount while at the same time, update the count to the
        UI.
        */
        GameObject.Find("ScoreText").GetComponent<ScoreTracker>().killCount++;
        GameObject.Find("ScoreText").GetComponent<ScoreTracker>().UpdateKillCounterUI();
        
        Destroy(gameObject);
    }

    /*
    The module below is just to get a visual representation of the enemy attacking you. 
    */
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackRange);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, sightRange);
    }
}